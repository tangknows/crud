class CommentsController < ApplicationController
  def new
  	@post = Post.find(params[:post_id])
  	@comment = Comment.new
  end

  def create
  	@post = Post.find(params[:post_id])
    @comment = Comment.new(comment_params)
    @comment.post_id = params[:post_id]
    if @comment.save
      flash[:notice] = "Comment was saved."
      redirect_to @post
    else
      flash[:error] = "There was an error saving the comment. Please try again."
      render :new
    end
  end

  def edit
    @post = Post.find(params[:post_id])
    @comment = Comment.find(params[:id])
  end

  def update
    puts params
    @post = Post.find(params[:post_id])
    @comment = Comment.find(params[:id])
    if @comment.update_attributes(params.require(:comment).permit(:body))
      flash[:notice] = "Comment was updated."
      redirect_to @post
    else
      flash[:error] = "There was an error saving the post. Please try again."
    end
  end

  def destroy
  	@post = Post.find(params[:post_id])
    @comment = Comment.find(params[:id])
    if @comment.destroy
      flash[:notice] = "Post was deleted."
      redirect_to @post
    else
      flash[:error] = "There was an error deleting the comment. Please try again."
      render :edit
    end
  end

  private
    def comment_params
      params.require(:comment).permit(:body)
    end
end
