class PostsController < ApplicationController
  def index
  	@posts = Post.all

  end

  def new
    @post = Post.new
  end

  def edit
    @post = Post.find(params[:id])
  end

  def show
  	@post = Post.find(params[:id])
    @comment = Comment.new
    @comment.post_id = @post.id
  end

  def create
    @post = Post.new(post_params)
    if @post.save
      flash[:notice] = "Post was saved."
      redirect_to @post
    else
      flash[:error] = "There was an error saving the post. Please try again."
      render :new
    end
  end

  def update
    @post = Post.find(params[:id])
    if @post.update_attributes(params.require(:post).permit(:title, :body))
      flash[:notice] = "Post was updated."
      redirect_to @post
    else
      flash[:error] = "There was an error saving the post. Please try again."
      render :edit
    end
  end

  def destroy
    @post = Post.find(params[:id])
    if @post.destroy
      flash[:notice] = "Post was deleted."
      redirect_to @post
    else
      flash[:error] = "There was an error deleting the post. Please try again."
      render :edit
    end
  end

  private
    def post_params
      params.require(:post).permit(:title, :body)
    end

end
