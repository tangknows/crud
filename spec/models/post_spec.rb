require 'rails_helper'

describe Post do
 
 describe "create post" do

   before do
     # @post = Post.create(title: 'this is title', body: 'this is body')
     @post = FactoryBot.create(:post)
     2.times { @post.comments.create(body: "test comment") }
   end

   describe 'create posts' do 
    
    it 'it created post with title and body' do
      expect( @post.title ).to eq('this is title')
      expect( @post.body ).to eq('this is body')
    end

    it 'it created post with title and body' do
      expect( @post.comments.first.body ).to eq('test comment')
    end

   end

 end

end

