require 'rails_helper'

describe PostsController do
  
  describe "POST index" do
    it 'returns http success' do
      get :index
      expect(response).to be_success
      expect(response).to have_http_status(200)
      response.should render_template :index
    end
  end

  describe "POST create" do
    it 'should create the post' do
      post :create, params:{ post: { title: 'this is title', body: 'this is body' } }
      post = Post.last
      expect(post.title).to eq('this is title')
      expect(post.body).to  eq('this is body')
    end
  end

  describe "POST show" do
    it 'should show the post' do
      @post = FactoryBot.create(:post)
      get :show, params: { id: @post.id }
      assert_response :success
      response.should render_template :show
    end
  end

  describe "POST new" do
    it 'should show the post' do
      get :new
      assert_response :success
      response.should render_template :new
    end
  end

end
